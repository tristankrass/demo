using System.ComponentModel.DataAnnotations;

namespace Domain
{
    public class Project
    {
        public int ProjectId { get; set; }

        [Required]
        [MinLength(2), MaxLength(50)]
        public string ProjectName { get; set; }

        [Required] public int Difficulty { get; set; }

        public string Description { get; set; }


        public string TechUsed { get; set; }
    }
}