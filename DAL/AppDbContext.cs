﻿using System;
using Domain;
using Microsoft.EntityFrameworkCore;

namespace DAL
{
    public class  AppDbContext : DbContext
    {

        public DbSet<Project> Projects { get; set; }
        
        public AppDbContext(DbContextOptions options) : base(options)
        {
        }
    }
}